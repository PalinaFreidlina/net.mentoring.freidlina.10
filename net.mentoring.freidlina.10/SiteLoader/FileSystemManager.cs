﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteLoader
{
    class FileSystemManager
    {
        public void AddDirectoryForLevel(string homeUrl, string url)
        {
            if (!url.Contains(homeUrl)) return;
            var directory = url.Substring(url.IndexOf(homeUrl, StringComparison.Ordinal) + homeUrl.Length);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        public static void SaveToFile(string file, string content)
        {
            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine(content);
                }
            }
        }
    }
}
