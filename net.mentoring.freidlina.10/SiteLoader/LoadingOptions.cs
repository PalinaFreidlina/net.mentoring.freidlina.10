﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteLoader
{
    public enum OtherDomainsLimitation
    {
        NoLimits,
        OnlyThisDomain,
        NotHigherThisUrl
    }
    public class LoadingOptions
    {
        public string Url { get; set; }
        public string DirectoryPath { get; set; }
        public int Depth { get; set; } = Int32.MaxValue;
        public OtherDomainsLimitation DomainLimit { get; set; } = OtherDomainsLimitation.OnlyThisDomain;
        public List<string> AllowedResoursesExtentions { get; set; } = new List<string>() {".css",".js"};
        public bool Trace { get; set; } = false;
        public string Domain {
            get
            {
                Uri uri = new Uri(Url);
                return uri.Host;
            }
        }
    }
}
