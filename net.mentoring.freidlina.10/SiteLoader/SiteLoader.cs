﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CsQuery;

namespace SiteLoader
{
    public class SiteLoader
    {
        private int depth = 0;
        private readonly LoadingOptions _options;
        private const string _HtmlExt = ".html";
        private List<string> visitedLinks = new List<string>();
        public SiteLoader(LoadingOptions options)
        {
            _options = options;
        }

        public void Load()
        {
            Load(_options.Url,_options.DirectoryPath);

        }

        private void Load(string url, string path)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(url).Result)
                {
                    using (HttpContent content = response.Content)
                    {
                        SaveResources(url, path, content.ReadAsStringAsync().Result);
                    }
                }
            }
        }
        

        private void SaveResource(string url, string path, string resource)
        {
            var directory = url.Contains(_options.Url)
                ? Path.Combine(path,
                    url.Substring(url.IndexOf(_options.Url, StringComparison.Ordinal) +
                                      _options.Url.Length).Replace("/", "\\"))
                : path;
            var file = Path.Combine(directory, GetResourceName(url));
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.WriteLine(resource);
                }
            }
            visitedLinks.Add(url);
        }

        private void SaveResources(string url, string path, string html)
        {
            if (depth > _options.Depth) return;
            SaveResource(url,path,html);

            CQ cq = CQ.Create(html);
            var links = cq["[href]:not([href='']):not([href^='//']):not([href^='/'])"]
                .Select(item => item.GetAttribute("href"));
            var content = cq["[src]:not([src='']):not(iframe):not([src^='//'])"]
                .Select(item => item.GetAttribute("src"));

            foreach (var link in links.Union(content))
            {
                var nextUrl = FilterUrl(link);
                if (nextUrl != url && !string.IsNullOrEmpty(nextUrl) && !visitedLinks.Contains(nextUrl))
                    Load(nextUrl, path);
            }
        }

        private void Trace(string message)
        {
            if(_options.Trace) System.Diagnostics.Trace.WriteLine(message);
        }

        private string ValidateUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return null;
            if (url.StartsWith("//") && !url.Contains("http:"))
                return "http:" + url;
            if (url.StartsWith("/"))
                return "http://" + _options.Domain + url;
            if (!url.StartsWith("http"))
                return null;
            return url;
        }

        private string FilterUrl(string url)
        {
            var resName = GetResourceName(url);
            var ext = resName.Contains(".") ? resName.Substring(resName.LastIndexOf('.')) : _HtmlExt;

            if (!_options.AllowedResoursesExtentions.Contains(ext) && ext != _HtmlExt) return null;
            switch (_options.DomainLimit)
            {
                case OtherDomainsLimitation.NotHigherThisUrl:
                    if (!url.Contains(_options.Url)) return null;
                    break;
                case OtherDomainsLimitation.OnlyThisDomain:
                    if (!url.Contains(_options.Domain)) return null;
                    break;
            }

            return ValidateUrl(url);
        }

        private string GetResourceName(string url)
        {
            var resName = url.Substring(url.LastIndexOf('/') + 1);
            int startQuery = resName.LastIndexOf('?');
            return startQuery >= 0 ? resName.Substring(0, resName.IndexOf('?')) : resName;
        }
    }
}
