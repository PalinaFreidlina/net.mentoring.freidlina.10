﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SiteLoader;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new LoadingOptions();
            Console.WriteLine("------------Site Loader-----------");
            Console.WriteLine("Insert URL: ");
            options.Url = @"http://pfreydlina.ucoz.net/";//Console.ReadLine();
            Console.WriteLine("Input directory path to save site: ");
            options.DirectoryPath = @"D:\Learn\Mentoring\Tasks\net.mentoring.freidlina.10\site";//Console.ReadLine();
            string opt;
            do
            {
                Console.WriteLine("Set options for loading:\n" + 
                "1. Set Depth \n" + 
                "2. Choose domain limit\n" +
                "3. Set allowed resourses extentions\n" +
                "4. Trace process\n" +
                "0. Start\n");
                opt = Console.ReadLine();
                switch (opt)
                {
                    case "0": break;
                    case "1": Console.WriteLine("Input depth: ");
                        int.TryParse(Console.ReadLine(), out var depth);
                        options.Depth = depth;
                        break;
                    case "2":
                        Console.WriteLine("Choose:\n" + 
                            "1. No limits\n" + 
                            "2. Only this Domain\n" + 
                            "3. Not higher than set URL");
                        var lim = Console.ReadLine();
                        switch (lim)
                        {
                            case "1": options.DomainLimit = OtherDomainsLimitation.NoLimits;
                                break;
                            case "2": options.DomainLimit = OtherDomainsLimitation.OnlyThisDomain;
                                break;
                            case "3": options.DomainLimit = OtherDomainsLimitation.NotHigherThisUrl;
                                break;
                        }
                        break;
                    case "3": Console.WriteLine("Input allowed extentions by pressing Enter. To finish, leave empty string and press Enter");
                        string ext;
                        do
                        {
                            ext = Console.ReadLine();
                            options.AllowedResoursesExtentions.Add(ext);
                        } while (!string.IsNullOrEmpty(ext));
                        break;
                    case "4": Console.WriteLine("Trace process?(y/n) ");
                        options.Trace = Console.ReadLine()?.ToLower() == "y";
                        break;
                    default: Console.WriteLine("Invalid input");
                        break;
                    
                }
            } while (opt!="0");

            new SiteLoader.SiteLoader(options).Load();
        }
    }
}
